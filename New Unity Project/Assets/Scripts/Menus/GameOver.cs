﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {
	public Texture bG;

	void OnGUI () {
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), bG);
		
		if(GUI.Button(new Rect(Screen.width * 0.25f, Screen.height * 0.5f, Screen.width * 0.5f, Screen.height * 0.25f), "Back to Main Menu")){
			Application.LoadLevel("MainMenu");
		}
	}
}
