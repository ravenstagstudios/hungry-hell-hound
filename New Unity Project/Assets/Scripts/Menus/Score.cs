﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
	public float totalTime;
	public int totalOrbs;
	public int deaths;
	public Texture tex;
	private float newScore;
	private string newName;
	private float tempScore;
	private string tempName;
	private bool entered;		//entered name?
	public string playerNameGet = "Type your name here";
	private string scoreStr;


	// Use this for initialization
	void Start () {
		newScore = ((totalOrbs - deaths) / totalTime * 1000);
		scoreStr = newScore.ToString ();

	}
	
	// Update is called once per frame
	void OnGUI () {
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), tex);
		//if name entered
		if(entered){
			//Score Table
			for(int i=0;i<10;i++){
				GUI.Box (new Rect(Screen.width * 0.2f, Screen.height * (0.25f + 0.06f * i), Screen.width * 0.1f, Screen.height * 0.05f), (i+1).ToString ());
				GUI.Box (new Rect(Screen.width * 0.32f, Screen.height * (0.25f + 0.06f * i), Screen.width * 0.3f, Screen.height * 0.05f), PlayerPrefs.GetString (i+"Name"));
				GUI.Box (new Rect(Screen.width * 0.64f, Screen.height * (0.25f + 0.06f * i), Screen.width * 0.2f, Screen.height * 0.05f), PlayerPrefs.GetFloat (i+"Score").ToString ());
			}

			//MainMenu Button
			if(GUI.Button(new Rect(Screen.width * 0.25f, Screen.height * 0.85f, Screen.width * 0.5f, Screen.height * 0.15f), "Play"))
				Application.LoadLevel("MainMenu");
		}
		//before name entered
		else{
			playerNameGet = GUI.TextField(new Rect(Screen.width * 0.25f, Screen.height * 0.3f, Screen.width * 0.5f, Screen.height * 0.1f), playerNameGet);
			//Display Score
			GUI.Button(new Rect(Screen.width * 0.25f, Screen.height * 0.5f, Screen.width * 0.5f, Screen.height * 0.1f), "Your Score was: " + scoreStr);
			if(GUI.Button(new Rect(Screen.width * 0.25f, Screen.height * 0.8f, Screen.width * 0.5f, Screen.height * 0.25f), "Continue")){
				newName = playerNameGet;
				UpdateScore();
				entered = true;
			}

		}
	}

	void UpdateScore(){
		for(int i=0;i<10;i++){
			//if i score exists check to see if newScore is higher
			if(PlayerPrefs.HasKey(i+"Score")){
				//if newscore higher than oldScore, overwrite and store oldScore/name as newScore/Name then continue loop
				if(newScore > PlayerPrefs.GetFloat (i+"Score")){
					tempName = PlayerPrefs.GetString (i+"Name");
					tempScore = PlayerPrefs.GetFloat (i+"Score");
					PlayerPrefs.SetString (i+"Name", newName);
					PlayerPrefs.SetFloat (i+"Score", newScore);
					newName = tempName;
					newScore = tempScore;
				}
			}
			//if score does not exist insert new score
			else{
				PlayerPrefs.SetFloat (i+"Score", newScore);
				PlayerPrefs.SetString (i+"Name", newName);
				newScore = 0;
				newName = "";
			}
			PlayerPrefs.Save ();
		}
	}
}

