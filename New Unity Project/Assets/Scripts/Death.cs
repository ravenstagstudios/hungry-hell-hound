﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour {
	private float initialX = 0f;
	private float initialY = 0f;

	private bool wall = false;		//is player in contact with left wall
	private bool platEdge = false;	//is player in contact with platform edge

	private platformCamera cam;
	private PhysicsRoll orbsReset;
	
	public bool respawn = false;


	void Awake () {
		initialX = transform.position.x;
		initialY = transform.position.y;
		//Debug.Log ("X = " + initialX);
		//Debug.Log ("Y = " + initialY);

		cam = GameObject.Find("Camera").GetComponent<platformCamera>();
	}

	void FixedUpdate () {
		if (wall && platEdge)
			Dead ();
	}
	//Kill on lava
	void OnTriggerEnter2D(Collider2D lava){

		if (lava.gameObject.tag == "Lava")
			Dead ();
	}



	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "platformEdge") {
			platEdge = true;
		}

		if (coll.gameObject.tag == "frameEdge") {
			wall = true;
		}
	}

	void OnCollisionExit2D(Collision2D coll) {
		if (coll.gameObject.tag == "platformEdge") {
			platEdge = false;
		}

		if (coll.gameObject.tag == "frameEdge") {
			wall = false;
		}
	}






	//run to respawn player an orbs
	void Dead () {
		//move player to original position
		transform.position = new Vector2 (initialX, initialY);
		//remove velocity
		rigidbody2D.velocity = new Vector2(0,0);
		//set respawn trigger to notify orb spawners
		respawn = true;
		//if in platformer section, reset camera
		if (gameObject.tag == "platformPlayer"){
			cam.dead = true;
			//and reset wall and platEdge
			wall = false;
			platEdge = false;

		}
		//reset orb count
		if  (GameObject.Find("Roll Player") != null){
			orbsReset = GameObject.Find("Roll Player").GetComponent<PhysicsRoll>();
			orbsReset.currentOrbs = 0;
			orbsReset.deaths++;
		}
	}

}
