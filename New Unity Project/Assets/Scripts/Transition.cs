﻿using UnityEngine;
using System.Collections;

public class Transition : MonoBehaviour {
	public bool transitionStart;
	public bool offeringEaten;
	public float moveForce = 200f;
	public float maxSpeed = 3;
	private float yForce = -50;
	//Check for >=5
	private platformCamera orbCount;
	//Destroys offering
	private OfferingDestroy dest;
	//Animator reference
	private Animator anim;
	//had player been moved to start of transition
	private bool moved = false;
	// Use this for initialization
	void Start () {
		dest = GameObject.Find ("Offering").GetComponent<OfferingDestroy> ();
		anim = GameObject.Find("PlatPlayer").GetComponent<Animator>();
		orbCount = GameObject.Find("Camera").GetComponent<platformCamera>();
	}

	void FixedUpdate() {


		if (transitionStart) {
			anim.SetFloat("Speed", 1);
			if (moved == false){
				Debug.Log(moved);
				rigidbody2D.velocity = new Vector2(0, yForce);
				GameObject.Find("PlatPlayer").GetComponent<MultiJump>().enabled = false;
				GameObject.Find("PlatPlayer").GetComponent<PhysicsPlat>().enabled = false;
				GameObject.Find("PlatPlayer").GetComponent<Death>().enabled = false;
				moved = true;
				rigidbody2D.fixedAngle = false;
			}


			if (rigidbody2D.velocity.x < maxSpeed)
				rigidbody2D.AddForce(Vector2.right * moveForce);
		}
	}
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D trig){
		if (trig.gameObject.tag == "Finish"){
			if(orbCount.orbs > 4)
			transitionStart = true;
			else
				Application.LoadLevel("GameOver");
		}
		if (trig.gameObject.tag == "LvlLoad"){
			transitionStart = false;
			Application.LoadLevel("StandaloneRollScene");
		}
		if (trig.gameObject.tag == "Offering"){
			anim.SetTrigger("Chomp2");
			dest.dest = true;
		}
	}

}
