﻿using UnityEngine;
using System.Collections;

public class BorderRight : MonoBehaviour {
	public Transform cam;

	void Start(){
		cam = GameObject.FindGameObjectWithTag ("MainCamera").transform;
	}

	//set border to screen width
	void FixedUpdate () {
		transform.position = new Vector2 (cam.position.x +(Camera.main.orthographicSize * Camera.main.aspect), cam.position.y);
	}
}