﻿using UnityEngine;
using System.Collections;

public class transitionCamera : MonoBehaviour {

	private Transform player;
	// Use this for initialization
	void Start () {
		player = GameObject.Find("PlatPlayer").transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.position = new Vector3 (player.position.x, player.position.y, transform.position.z);
	}

}
