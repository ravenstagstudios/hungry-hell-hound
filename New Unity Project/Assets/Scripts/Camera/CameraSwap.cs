﻿using UnityEngine;
using System.Collections;

public class CameraSwap : MonoBehaviour {
	private rollCameraFollow roll;
	private platformCamera plat;
	private transitionCamera transCam;
	private Transition trans;
	private bool transition;
	private CameraGUI timeStop;
	private platformCamera orbs;
	public int midOrbs;
	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad(GameObject.Find("Camera"));
		timeStop = GameObject.Find ("Camera").GetComponent<CameraGUI> ();

		if (GameObject.Find("PlatPlayer") != null){
			trans = GameObject.Find("PlatPlayer").GetComponent<Transition>();
			GameObject.Find("Camera").GetComponent<rollCameraFollow>().enabled = false;
		}
		else {
			GameObject.Find("Camera").GetComponent<platformCamera>().enabled = false;
		}


		GameObject.Find ("Camera").GetComponent<transitionCamera> ().enabled = false;
		//roll.enabled = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (GameObject.Find("PlatPlayer") != null){
		transition = trans.transitionStart;
		}
		if (GameObject.Find("Roll Player") != null){
			GameObject.Find("Camera").GetComponent<platformCamera>().enabled = false;
			GameObject.Find("Camera").GetComponent<transitionCamera>().enabled = false;
			GameObject.Find("Camera").GetComponent<rollCameraFollow>().enabled = true;
		}

		if (transition){
			transition = false;
			timeStop.timeStop = true;
			orbs = GameObject.Find("Camera").GetComponent<platformCamera>();
			midOrbs = orbs.orbs;
			GameObject.Find("Camera").GetComponent<platformCamera>().enabled = false;
			GameObject.Find("Camera").GetComponent<transitionCamera>().enabled = true;

		}
	}
}
