﻿using UnityEngine;
using System.Collections;

public class CameraGUI : MonoBehaviour {
	private string orbCount;
	private platformCamera pCam;
	public Texture orb;		//plat orb icon (sheep)
	public Texture orb2;	//roll orb icon (cat)
	public float platTotalTime = 0f;		//Time taken for first section
	public float currentTime = 0f;		//Time taken on curent run
	private int simpleTime;
	private string timerDisplay;
	private Death death;
	public bool timeStop;
	private PhysicsRoll rollOrbs;
	public int deathCounter;

	void Start () {
		if (GameObject.Find("PlatPlayer") != null){
			death = GameObject.Find ("PlatPlayer").GetComponent <Death>();
		}
		else if (GameObject.Find("RollPlayer") != null){
			death = GameObject.Find ("Roll Player").GetComponent <Death>();
			rollOrbs = GameObject.Find ("Roll Player").GetComponent <PhysicsRoll>();
		}


		pCam = GameObject.Find ("Camera").GetComponent < platformCamera>();
	}

	void Update() {
		currentTime += Time.deltaTime;
	}

	void FixedUpdate(){
		if (timeStop)
			platTotalTime = currentTime;


		if (GameObject.Find("PlatPlayer") != null){
			orbCount = pCam.orbs.ToString();
			if(death.respawn)
				Respawn();
		}
		else if (GameObject.Find("Roll Player") != null){
			orbCount = rollOrbs.orbs.ToString();
			if(death.respawn)
				Respawn();

		}




		simpleTime = (int) currentTime;
		timerDisplay = simpleTime.ToString ();
	}


	void OnGUI () {

		//Orb Counter
		GUI.Button (new Rect (Screen.width * 0.1f, Screen.height * 0.075f, Screen.width * 0.1f, Screen.height * 0.05f), "Critters Consumed: " +orbCount);
		//Orb Icon
		//if plat show sheep
		if  (GameObject.Find("PlatPlayer") != null)
		GUI.DrawTexture(new Rect(Screen.width * 0.05f, Screen.height * 0.05f, Screen.width * 0.05f, Screen.height * 0.1f), orb, ScaleMode.ScaleToFit);
		//else show cat
		else{
			GUI.DrawTexture(new Rect(Screen.width * 0.05f, Screen.height * 0.05f, Screen.width * 0.05f, Screen.height * 0.1f), orb2, ScaleMode.ScaleToFit);
			//Death Counter
			GUI.Button (new Rect (Screen.width * 0.7f, Screen.height * 0.075f, Screen.width * 0.1f, Screen.height * 0.05f), "Deaths: " + rollOrbs.deaths.ToString ());
		}
		//Timer
		GUI.Button (new Rect (Screen.width * 0.8f, Screen.height * 0.075f, Screen.width * 0.1f, Screen.height * 0.05f), "Time: " + timerDisplay);



		if(GUI.Button(new Rect(Screen.width * 0.4f, Screen.height * 0.05f, Screen.width * 0.2f, Screen.height * 0.075f), "Quit")){
			Application.LoadLevel("Tutorial");
		}

	}

	void Respawn(){
		currentTime = 0;

	}

	void OnLevelWasLoaded() {
		Respawn ();
		if (GameObject.Find("Roll Player") != null){
			death = GameObject.Find ("Roll Player").GetComponent <Death>();
			rollOrbs = GameObject.Find ("Roll Player").GetComponent <PhysicsRoll>();
		}
	}
}
