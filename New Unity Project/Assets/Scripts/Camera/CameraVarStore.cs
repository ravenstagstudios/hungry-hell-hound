﻿using UnityEngine;
using System.Collections;

public class CameraVarStore : MonoBehaviour {
	private PhysicsRoll rollOrbs;
	private CameraGUI timers;

	private Score score;

	public int finalOrbs;
	public float time1;
	public float time2;
	public float timeTotal;
	public int deaths;


	void FixedUpdate(){
		if (GameObject.Find("Roll Player") != null){
			if (rollOrbs.goal){
				time1 = timers.platTotalTime;
				time2 = timers.currentTime;
				finalOrbs = rollOrbs.orbs;
				deaths = rollOrbs.deaths;
				GameObject.Find("Camera").GetComponent<rollCameraFollow>().enabled = false;
				GameObject.Find("Camera").GetComponent<CameraSwap>().enabled = false;
				GameObject.Find("Camera").GetComponent<CameraGUI>().enabled = false;
				Application.LoadLevel("Score");

			}
		}
	}
	
	void OnLevelWasLoaded () {
		if (GameObject.Find("Roll Player") != null){
			timers = GameObject.Find("Camera").GetComponent<CameraGUI>();
			rollOrbs = GameObject.Find("Roll Player").GetComponent<PhysicsRoll>();
		}
		if (GameObject.Find("ScoreCamera") != null){
			score = GameObject.Find("ScoreCamera").GetComponent<Score>();
			score.totalOrbs = finalOrbs;
			score.totalTime = time1 + time2;
			score.deaths = deaths;
			GameObject.Destroy(this.gameObject, 0);
		}
		//destroy camera if quit
		if (GameObject.Find("Tutorial") != null || GameObject.Find ("GameOver") != null){
			GameObject.Destroy(this.gameObject, 0);
		}
	
	}
}
