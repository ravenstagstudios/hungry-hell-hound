﻿using UnityEngine;
using System.Collections;

public class RollOrb : MonoBehaviour {

	private PhysicsRoll roll;

	void Start () {

	}

	void Awake () {
		roll = GameObject.Find ("Roll Player").GetComponent<PhysicsRoll>();
	}


	void OnTriggerEnter2D(Collider2D orb) {
		if (orb.gameObject.tag == "Orb") {
			roll.currentOrbs++;
		}
	}
}
