﻿using UnityEngine;
using System.Collections;

public class OrbSpawn : MonoBehaviour {

	public GameObject originalOrb;
	private Vector3 spawnPos;
	public bool orbDestroyed = false;
	private bool respawn = false;

	private OrbRemove rem;
	private Death death;


	void Start () {

		if (GameObject.Find("PlatPlayer") != null){
			death = GameObject.Find("PlatPlayer").GetComponent<Death>();
		}
		else if (GameObject.Find("Roll Player") != null){
			death = GameObject.Find("Roll Player").GetComponent<Death>();
		}
		spawnPos = new Vector3 (transform.position.x, transform.position.y, 0);
		//originalOrb = GameObject.Find("OrbOriginal");



		GameObject clone = (GameObject) Instantiate (originalOrb, spawnPos, Quaternion.identity);

		rem = clone.GetComponent<OrbRemove>();
	}

	void FixedUpdate () {
		if (respawn && death.respawn)
			death.respawn = false;

		respawn = death.respawn;

		orbDestroyed = rem.orbDestroyed;


		if(respawn){
			if (orbDestroyed){
				GameObject clone;
				clone = (GameObject) Instantiate (originalOrb, spawnPos, Quaternion.identity);
				rem = clone.GetComponent<OrbRemove>();
				orbDestroyed = false;
			}

		}
	}
}
