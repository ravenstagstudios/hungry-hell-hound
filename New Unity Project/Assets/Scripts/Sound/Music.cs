﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {
	public AudioClip clip1;
	public AudioClip clip2;

	void Awake(){
		DontDestroyOnLoad(GameObject.Find("Music"));
	}

	void OnLevelWasLoaded () {
		if (GameObject.Find ("Roll Player") != null){
			audio.Stop();
			audio.clip = clip2;
			audio.Play();
		}


		if (GameObject.Find ("Roll Player") == null && audio.clip != clip1){
			audio.Stop();
			audio.clip = clip1;
			audio.Play();
		}


		if (GameObject.Find ("ScoreCamera") != null || GameObject.Find ("GameOver") != null){
			GameObject.Destroy(this.gameObject, 0);
		}
	}


}
