﻿using UnityEngine;
using System.Collections;

public class Transition : MonoBehaviour {
	public bool transitionStart;
	public bool offeringEaten;
	public float moveForce = 200f;
	public float maxSpeed = 3;
	private float yForce = -50;

	private OfferingDestroy dest;

	private Animator anim;


	private bool moved = false;
	// Use this for initialization
	void Start () {
		dest = GameObject.Find ("Offering").GetComponent<OfferingDestroy> ();
		anim = GameObject.Find("PlatPlayer").GetComponent<Animator>();
		Debug.Log (moved);
	}

	void FixedUpdate() {
		anim.SetFloat("Speed", 1);

		if (transitionStart) {
			if (moved == false){
				Debug.Log(moved);
				rigidbody2D.velocity = new Vector2(0, yForce);
				GameObject.Find("PlatPlayer").GetComponent<MultiJump>().enabled = false;
				GameObject.Find("PlatPlayer").GetComponent<PhysicsPlat>().enabled = false;
				GameObject.Find("PlatPlayer").GetComponent<Death>().enabled = false;
				moved = true;
			}


			if (rigidbody2D.velocity.x < maxSpeed)
				rigidbody2D.AddForce(Vector2.right * moveForce);
		}
	}
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D trig){
		if (trig.gameObject.tag == "Finish"){
			transitionStart = true;
		}
		if (trig.gameObject.tag == "LvlLoad"){
			transitionStart = false;
			Application.LoadLevel("RollScene");
		}
		if (trig.gameObject.tag == "Offering"){
			anim.SetTrigger("Chomp");
			dest.dest = true;
		}
	}

}
