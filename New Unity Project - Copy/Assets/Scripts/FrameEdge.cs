﻿using UnityEngine;
using System.Collections;

public class FrameEdge : MonoBehaviour {
	private Transform cam;
	
	void Awake () {
		cam = GameObject.FindGameObjectWithTag ("MainCamera").transform;
	}
	

	void FixedUpdate () {
		transform.position = new Vector2 (cam.position.x, cam.position.y);
	}
}
