﻿using UnityEngine;
using System.Collections;

public class PhysicsPlat : MonoBehaviour {
	public float maxSpeed = 5f;
	public float moveForce = 300f;

	private bool faceRight;
	private Animator anim;



	//Temporarily removed because of lack of animation
	//private Animator anim;

	void Start () {

		anim = GameObject.Find("PlatPlayer").GetComponent<Animator>();
	}
	

	void FixedUpdate () {

		//Debug.Log("Velocity = " + rigidbody2D.velocity.x);

		//Horizontal input
		float horiz = Input.GetAxis ("Horizontal");
		//Change Speed parameter to determine which animation to use
		anim.SetFloat("Speed", Mathf.Abs (rigidbody2D.velocity.x));
		Debug.Log (Mathf.Abs (rigidbody2D.velocity.x));
		//Adding movement force
		if (rigidbody2D.velocity.x * horiz < maxSpeed) 
			rigidbody2D.AddForce(Vector2.right * horiz * moveForce);
		if (Mathf.Abs (rigidbody2D.velocity.x) > maxSpeed)
						rigidbody2D.velocity = new Vector2 (Mathf.Sign (rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);

		//Flip
		if (horiz > 0 && faceRight)
			Flip ();
		if (horiz < 0 && !faceRight)
			Flip ();




	}

	void Flip () {
		faceRight = !faceRight;

		//Multiply X scale by -1
		Vector3 tempScale = transform.localScale;
		tempScale.x *= -1;
		transform.localScale = tempScale;
	}
}
