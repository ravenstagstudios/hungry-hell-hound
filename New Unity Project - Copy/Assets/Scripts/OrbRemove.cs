﻿using UnityEngine;
using System.Collections;

public class OrbRemove : MonoBehaviour {
	public bool orbDestroyed = false;
	private Animator anim;

	void Start() {
		if (GameObject.Find ("PlatPlayer") != null){
		anim = GameObject.Find("PlatPlayer").GetComponent<Animator>();
		}
	}

	void OnTriggerEnter2D(Collider2D thisOrb){
			if (GameObject.Find("PlatPlayer") != null){
			if (thisOrb.gameObject.tag == "platformPlayer" ) {
				orbDestroyed = true;
				anim.SetTrigger("Chomp");
				GameObject.Destroy(this.gameObject, 0);
			}
		}
		if (GameObject.Find("Roll Player") != null){
			if (thisOrb.gameObject.tag == "rollPlayer" ) {
				orbDestroyed = true;
				GameObject.Destroy(this.gameObject, 0);
			}
		}

		
	}
}


/*if (GameObject.Find("PlatPlayer") != null){

		}
		else if (GameObject.Find("RollPlayer") != null){

		}
		*/
