﻿using UnityEngine;
using System.Collections;

public class PhysicsRoll : MonoBehaviour {
	public float constantThrust = 10;
	private bool grounded = false;
	public int currentOrbs;
	public int initialOrbs;
	public int orbs;				//Amount of orbs collected in total
	private CameraSwap initOrb;
	public bool goal;

	public StandaloneRollCamera startOrbs;		//Temp for standalone camera


	void Start () {
		if (GameObject.Find("Camera") != null){
		initOrb = GameObject.Find("Camera").GetComponent<CameraSwap>();
		initialOrbs = initOrb.midOrbs;
		}

		//temp solution to allow a standalone roll level for testing purposes
		if (GameObject.Find("RollCamera") != null){
			startOrbs = GameObject.Find("RollCamera").GetComponent<StandaloneRollCamera>();
			initialOrbs = startOrbs.startingOrbs;
		}

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		orbs = initialOrbs + currentOrbs;
	
		constantThrust = orbs * 2;		//Updates in case orbs.cs changes orbs value
		if(grounded)
			rigidbody2D.AddForce (Vector2.right * (constantThrust));
		Debug.Log (orbs);
	}
	
	
	void OnCollisionEnter2D(Collision2D plat) {
		if (plat.gameObject.tag == "Platform") {
			grounded = true;
			

		}
	}
	
	
	void OnCollisionExit2D(Collision2D plat) {
		if (plat.gameObject.tag == "Platform") {
			grounded = false;
		}
	}

	void OnTriggerEnter2D(Collider2D end){
		Debug.Log ("trig");
		if (end.gameObject.tag == "Catcher")
			Catch();
		if(end.gameObject.tag == "Goal")
			Goal();
	}

	void Catch(){
		rigidbody2D.velocity = new Vector2 (0, 0);
		transform.position = new Vector2 (779, transform.position.y);

	}

	void Goal() {
		goal = true;
	}
}
