﻿using UnityEngine;
using System.Collections;

public class MultiJump : MonoBehaviour {

	public float jumpThrust;					//Thrust of initial jump
	public float multiJumpThrust;				//Thrust of jumps after first
	private bool grounded = false;				//is player in contact with platform
	private bool jump = false;					//Player is attempting to jump
	public int multiJumpTotal;					//Total amount of multi jumps
	private int multiJump;						//Amount of multiJumps used in current jump
	public int initialExitFrames = 15;			//total frames multiJump can happen in
	private int exitFrames = 0;					//Frames since last contact with platform
	public int initialFrameDelay = 3;			//Frames between multijump
	private int frameDelay = 0;					//Frames since last multijump
	private bool startJump = false;				//is player currently jumping
	public int exitBuffer = 10;					//Frames after leaving platform when you can still jump
	private Animator anim;
	private int landingCounter;


	//Jump sound clips
	public AudioClip rollJumpSFX;
	public AudioClip platJumpSFX;



	void Start(){


		if (gameObject.tag == "rollPlayer") {
			jumpThrust = 300f;
			multiJumpThrust = 200f;
			multiJumpTotal = 5;
			Debug.Log ("rollPlayer");
		} 
		else if (gameObject.tag == "platformPlayer") {
			jumpThrust = 400f;
			multiJumpThrust = 200f;
			multiJumpTotal = 3;
			anim = GameObject.Find("PlatPlayer").GetComponent<Animator>();
			Debug.Log ("platformPlayer");
		}
		else {
			Debug.Log("Player is not tagged");
		}
		multiJump = multiJumpTotal;
	}

	void Update ()
	{
		if (Input.GetButton ("Jump")) {
			if (exitFrames > exitBuffer) {
				jump = true;
			}
			else if (startJump == true && exitFrames > 0){
				jump = true;
			}

			//Debug.Log("Jump = True");
		}
		else{
			jump = false;
			//Stops audio
			if(startJump)
				audio.Stop ();
		}
	}


	void FixedUpdate ()
	{



		if (exitFrames > 0 && grounded == false)
			exitFrames--;


		if (startJump == true) {
			if (exitFrames == 0){
				startJump = false;
				multiJump = multiJumpTotal;
				frameDelay = 0;
			}

			if (frameDelay > 0)
				frameDelay--;
			if (multiJump == 0)
				exitFrames = 0;



		}


		if (jump && frameDelay == 0 && multiJump > 0 && exitFrames > 0) {

			if (multiJump == multiJumpTotal){
				rigidbody2D.AddForce (Vector2.up * jumpThrust);
				multiJump--;
				frameDelay = initialFrameDelay;
				startJump = true;
				exitFrames = initialExitFrames;
				//Play JumpSFX
				JumpSFX();


				if (GameObject.Find ("PlatPlayer") != null)
					anim.SetTrigger("Jump");

			}
			else {
				rigidbody2D.AddForce (Vector2.up * multiJumpThrust);
				multiJump--;
				frameDelay = initialFrameDelay;
			}
		}
	}


	void OnCollisionEnter2D(Collision2D plat) {
		if (plat.gameObject.tag == "Platform") {
			grounded = true;

			if (GameObject.Find("PlatPlayer") != null) {
				anim.SetBool("Landed", true);
			}


			if (startJump == false){
				exitFrames = initialExitFrames;
			}
		}
	}
	

	void OnCollisionExit2D(Collision2D plat) {
		if (plat.gameObject.tag == "Platform") {
			grounded = false;
			landingCounter = 0;

			if (GameObject.Find("PlatPlayer") != null) {
				anim.SetBool("Landed", false);
			}
		}
	}

	void JumpSFX() {
		if(GameObject.Find("Roll Player") != null){
			audio.clip = rollJumpSFX;
			audio.Play ();
		}
		else if(GameObject.Find("PlatPlayer") != null){
			audio.clip = platJumpSFX;
			audio.Play ();
		}

	}


}
