﻿using UnityEngine;
using System.Collections;

public class rollCameraFollow : MonoBehaviour {

	private Transform player;			// Reference to the player's transform.

	//Offset from player position
	public float posXOffset = 15;
	public float posYOffset = -4;

	void Awake() {
		if (GameObject.Find("Roll Player") != null)
			player = GameObject.FindGameObjectWithTag ("rollPlayer").transform;
	}

	void OnLevelWasLoaded () {
		if (GameObject.Find("Roll Player") != null)
		player = GameObject.FindGameObjectWithTag ("rollPlayer").transform;
	}
	

	void FixedUpdate () {
		float targetX = player.position.x + posXOffset;
		float targetY = player.position.y + posYOffset;

		transform.position = new Vector3(targetX, targetY, transform.position.z);

	}
}
