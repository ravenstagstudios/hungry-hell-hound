﻿using UnityEngine;
using System.Collections;

public class CameraGUI : MonoBehaviour {
	private string orbCount;
	private platformCamera pCam;
	public Texture orb;
	public float platTotalTime = 0f;		//Time taken for first section
	public float currentTime = 0f;		//Time taken on curent run
	private int simpleTime;
	private string timerDisplay;
	private Death death;
	public bool timeStop;
	private PhysicsRoll rollOrbs;

	void Start () {
		if (GameObject.Find("PlatPlayer") != null){
			death = GameObject.Find ("PlatPlayer").GetComponent <Death>();
		}
		else if (GameObject.Find("RollPlayer") != null){
			death = GameObject.Find ("Roll Player").GetComponent <Death>();
			rollOrbs = GameObject.Find ("Roll Player").GetComponent <PhysicsRoll>();
		}


		pCam = GameObject.Find ("Camera").GetComponent < platformCamera>();
	}

	void Update() {
		currentTime += Time.deltaTime;
	}

	void FixedUpdate(){
		if (timeStop)
			platTotalTime = currentTime;


		if (GameObject.Find("PlatPlayer") != null){
			orbCount = pCam.orbs.ToString();
			if(death.respawn)
				Respawn();
		}
		else if (GameObject.Find("Roll Player") != null){
			orbCount = rollOrbs.orbs.ToString();
			if(death.respawn)
				Respawn();

		}




		simpleTime = (int) currentTime;
		timerDisplay = simpleTime.ToString ();
	}


	void OnGUI () {
		GUI.Button (new Rect (Screen.width * 0.1f, Screen.height * 0.075f, Screen.width * 0.075f, Screen.height * 0.05f), "Sheep: " +orbCount);						//Orb Counter		
		GUI.DrawTexture(new Rect(Screen.width * 0.05f, Screen.height * 0.05f, Screen.width * 0.05f, Screen.height * 0.1f), orb, ScaleMode.ScaleToFit);	//Orb Icon

		//GUI.Button (new Rect (Screen.width * 0.9f, Screen.height * 0.075f, Screen.width * 0.05f, Screen.height * 0.05f), timerDisplay);
		GUI.Button (new Rect (Screen.width * 0.8f, Screen.height * 0.075f, Screen.width * 0.1f, Screen.height * 0.05f), "Time: " + timerDisplay);
	}

	void Respawn(){
		currentTime = 0;

	}

	void OnLevelWasLoaded() {
		Respawn ();
		if (GameObject.Find("Roll Player") != null){
			death = GameObject.Find ("Roll Player").GetComponent <Death>();
			rollOrbs = GameObject.Find ("Roll Player").GetComponent <PhysicsRoll>();
		}
	}
}
