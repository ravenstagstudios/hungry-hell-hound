﻿using UnityEngine;
using System.Collections;

public class platformCamera : MonoBehaviour {

	//Units moved per update
	public float baseScrollRate = 0.01f;	
	private float scrollRate = 0f;
	public int orbs = 0;					//Current amout of orbs
	public int previousOrbs = 0;			//Orbs in last frame
	public bool dead = false;
	private float initialX;
	private float initialY;

	void Awake () {
		scrollRate = baseScrollRate;

		initialX = transform.position.x;
		initialY = transform.position.y;
	}

	void FixedUpdate () {
		float targetX = transform.position.x + scrollRate;
		float targetY = transform.position.y + scrollRate;

		transform.position = new Vector3(targetX, targetY, transform.position.z);

		//Modifying scrollRate


		if (orbs != previousOrbs){
			scrollRate = baseScrollRate + orbs * 0.003f;
			previousOrbs++;

		}

		if (dead) {
			dead = false;
			Revert ();
		}
	}


	void Revert () {
		transform.position = new Vector3 (initialX, initialY, transform.position.z);
		orbs = 0;
		previousOrbs = -1;
	}
}
