﻿using UnityEngine;
using System.Collections;

public class PlatformerOrb : MonoBehaviour {

	
	private platformCamera plat;

	void Awake () {
		plat = GameObject.Find ("Camera").GetComponent<platformCamera>();
	}

	void OnTriggerEnter2D(Collider2D orb) {
		if (orb.gameObject.tag == "Orb") {
			plat.orbs++;
		}
}
}
