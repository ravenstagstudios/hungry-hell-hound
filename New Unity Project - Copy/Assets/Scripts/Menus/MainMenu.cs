﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public Texture tex;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnGUI () {
		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), tex);

		if(GUI.Button(new Rect(Screen.width * 0.5f, Screen.height * 0.5f, Screen.width * 0.25f, Screen.height * 0.25f), "Play")){
			Application.LoadLevel("Tutorial");
		}
	}
}
